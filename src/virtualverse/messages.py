from collections import namedtuple


LoginRequestMessage = namedtuple(
    "LoginRequestMessage", "timestamp username password version"
)
LoginResponseMessage = namedtuple(
    "LoginResponseMessage", "timestamp username status text"
)
LogoutRequestMessage = namedtuple("LogoutRequestMessage", "timestamp username")
LogoutResponseMessage = namedtuple("LogoutResponseMessage", "timestamp username")

AvatarPositionMessage = namedtuple(
    "AvatarPositionMessage", "timestamp username position"
)
AvatarSkinMessage = namedtuple("AvatarSkinMessage", "timestamp username skin")
WorldModifyMessage = namedtuple(
    "WorldModifyMessage", "timestamp username funcname args kwargs"
)
ChatPrivateMessage = namedtuple(
    "ChatPrivateMessage", "timestamp username receiver text"
)
ChatBroadcastMessage = namedtuple("ChatBroadcastMessage", "timestamp username text")
VoiceMessage = namedtuple("VoiceMessage", "timestamp username data")

ConfigMessage = namedtuple("ConfigMessage", "timestamp username config")
"""
config = {
    'blocks': {
        <position>: <texture>,
        <position>: <texture>,
    },
    'block-textures': {
        <texture_name>: <texture_data>,
        <texture_name>: <texture_data>,
    },
    'skin-textures': {
        <texture_name>: <texture_data>,
        <texture_name>: <texture_data>,
    },
}
"""
