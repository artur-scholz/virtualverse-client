import os
import time
# import argparse
import pyglet

from .window import Window
from .client import Client


def run():
    # parser = argparse.ArgumentParser()
    # parser.add_argument("username")
    # parser.add_argument("password")
    # parser.add_argument("--host", default="localhost")
    # parser.add_argument("--port", "-p", default=30000)
    # args = parser.parse_args()
    # client = Client(args.username, args.password, args.host, int(args.port))

    print()
    print("Hello to VirtualVerse!")
    print()
    username = input("username: ")
    password = input("password (Enter for none): ") 
    if password == "":
        password = "xxx"
    host = input("host (Enter for default): ")
    if host == "":
        host = "45.145.224.134"
    port = 30000
    client = Client(username, password, host, port)

    time.sleep(0.5)  # wait for audio warnings to complete

    client.login()

    retries = 0
    while not client.is_logged_in():
        time.sleep(0.5)
        retries += 1
        if retries > 10:
            print("No response from server")
            os._exit(0)

    if client.is_logged_in():
        while not client.initialized:
            time.sleep(1)
        client.configure()
        window = Window(client, width=800, height=600, resizable=True)
        # window.set_exclusive_mouse(True)
        pyglet.app.run()

        client.logout()
        time.sleep(0.5)  # HACK: give time to send logout message
        client.shutdown()


if __name__ == "__main__":
    run()
