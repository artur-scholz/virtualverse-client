import math

import pyglet
from pyglet import gl
from pyglet.window import key, mouse

from .config import (
    TICKS_PER_SEC,
    FLYING_SPEED,
    WALKING_SPEED,
    GRAVITY,
    TERMINAL_VELOCITY,
    PLAYER_HEIGHT,
    FACES,
    JUMP_SPEED,
)
from .utils import sectorize, normalize, cube_vertices, tex_coords


class Window(pyglet.window.Window):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent

        self.exclusive = False
        self.flying = False

        # strafing is moving lateral to the direction you are facing
        # strafe[0]: -1 when moving forward, 1 when moving back, 0 otherwise
        # strafe[1]: -1 when moving left, 1 when moving right, 0 otherwise
        self.strafe = [0, 0]

        # First element is rotation of the player in the x-z plane (ground
        # plane) measured from the z-axis down. The second is the rotation
        # angle from the ground plane up. Rotation is in degrees.
        # The vertical plane rotation ranges from -90 (straight down) to
        # 90 (looking straight up). The horizontal rotation range is unbounded.
        self.rotation = (0, 0)

        self.sector = None  # sector the player is currently in
        self.reticle = None  # crosshairs at the center of the screen
        self.dy = 0  # velocity in the y (upward) direction

        self.jumping = False

        # The current block the user can place. Hit num keys to cycle.
        self.block = self.parent.world.inventory[0]

        # Convenience list of num keys.
        self.num_keys = [
            key._1,
            key._2,
            key._3,
            key._4,
            key._5,
            key._6,
            key._7,
            key._8,
            key._9,
            key._0,
        ]

        # The info text that is displayed in the top left of the canvas.
        self.info = pyglet.text.Label(
            "",
            font_name="Arial",
            font_size=18,
            x=10,
            y=self.height - 10,
            anchor_x="left",
            anchor_y="top",
            color=(0, 0, 0, 255),
            multiline=True,
            width=1000,
        )
        self.info.color = (255, 255, 255, 255)

        # This call schedules the `update()` method to be called
        # TICKS_PER_SEC. This is the main game event loop.
        pyglet.clock.schedule_interval(self.update, 1.0 / TICKS_PER_SEC)

        self.setup_opengl()

    def setup_opengl(self):
        gl.glClearColor(0.5, 0.69, 1.0, 1)  # color of the sky, in rgba
        # enable culling (not rendering) of back-facing (invisible) facets
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        self.setup_fog()

    def setup_fog(self):
        gl.glEnable(gl.GL_FOG)
        # Set the fog color.
        gl.glFogfv(gl.GL_FOG_COLOR, (gl.GLfloat * 4)(0.5, 0.69, 1.0, 1))
        # no preference between rendering speed and quality
        gl.glHint(gl.GL_FOG_HINT, gl.GL_DONT_CARE)
        # specify the equation used to compute the blending factor
        gl.glFogi(gl.GL_FOG_MODE, gl.GL_LINEAR)
        # how close and far away fog starts and ends. The closer the start and
        # end, the denser the fog in the fog range.
        gl.glFogf(gl.GL_FOG_START, 20.0)
        gl.glFogf(gl.GL_FOG_END, 60.0)

    def set_exclusive_mouse(self, exclusive):
        super(Window, self).set_exclusive_mouse(exclusive)
        self.exclusive = exclusive

    def get_sight_vector(self):
        """Returns the current line of sight vector indicating the direction
        the player is looking.

        """
        x, y = self.rotation
        # y ranges from -90 to 90, or -pi/2 to pi/2, so m ranges from 0 to 1
        # and is 1 when looking ahead parallel to the ground and 0 when looking
        # straight up or down.
        m = math.cos(math.radians(y))
        # dy ranges from -1 to 1 and is -1 when looking straight down and 1
        # when looking straight up.
        dy = math.sin(math.radians(y))
        dx = math.cos(math.radians(x - 90)) * m
        dz = math.sin(math.radians(x - 90)) * m
        return (dx, dy, dz)

    def get_motion_vector(self):
        """Returns the current motion vector indicating the velocity of the
        player.

        Returns
        -------
        vector : tuple of len 3
            Tuple containing the velocity in x, y, and z respectively.

        """
        if any(self.strafe):
            x, y = self.rotation
            strafe = math.degrees(math.atan2(*self.strafe))
            y_angle = math.radians(y)
            x_angle = math.radians(x + strafe)
            if self.flying:
                m = math.cos(y_angle)
                dy = math.sin(y_angle)
                if self.strafe[1]:
                    # Moving left or right.
                    dy = 0.0
                    m = 1
                if self.strafe[0] > 0:
                    # Moving backwards.
                    dy *= -1
                # When you are flying up or down, you have less left and right
                # motion.
                dx = math.cos(x_angle) * m
                dz = math.sin(x_angle) * m
            else:
                dy = 0.0
                dx = math.cos(x_angle)
                dz = math.sin(x_angle)
        else:
            dy = 0.0
            dx = 0.0
            dz = 0.0
        return (dx, dy, dz)

    def update(self, dt):
        """This method is scheduled to be called repeatedly by the pyglet
        clock.

        Parameters
        ----------
        dt : float
            The change in time since the last call.

        """
        self.parent.world.process_queue()
        sector = sectorize(self.parent.avatar.position)
        if sector != self.sector:
            self.parent.world.change_sectors(self.sector, sector)
            if self.sector is None:
                self.parent.world.process_entire_queue()
            self.sector = sector
        m = 8
        dt = min(dt, 0.2)
        for _ in range(m):
            self._update(dt / m)

    def _update(self, dt):
        """Private implementation of the `update()` method. This is where most
        of the motion logic lives, along with gravity and collision detection.

        Parameters
        ----------
        dt : float
            The change in time since the last call.

        """
        # walking
        speed = FLYING_SPEED if self.flying else WALKING_SPEED
        d = dt * speed  # distance covered this tick.
        dx, dy, dz = self.get_motion_vector()
        # New position in space, before accounting for gravity.
        dx, dy, dz = dx * d, dy * d, dz * d
        # gravity
        if not self.flying:
            # Update your vertical speed: if you are falling, speed up until
            # you hit terminal velocity; if you are jumping, slow down until
            # you start falling.
            self.dy -= dt * GRAVITY
            self.dy = max(self.dy, -TERMINAL_VELOCITY)
            dy += self.dy * dt

        # collisions
        x, y, z = self.parent.avatar.position
        x, y, z = self.collide((x + dx, y + dy, z + dz), PLAYER_HEIGHT)
        self.parent.avatar.position = (x, y, z)

        if self.jumping and self.dy == 0:
            self.dy = JUMP_SPEED

        # if player is moving, send out its position
        min_d = 1e-3  # filter out noise
        if abs(dx) > min_d or abs(dy) > min_d or abs(dz) > min_d:
            self.parent.avatar.send_position()

    def collide(self, position, height):
        """Checks to see if the player at the given `position` and `height`
        is colliding with any blocks in the world.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position to check for collisions at.
        height : int or float
            The height of the player.

        Returns
        -------
        position : tuple of len 3
            The new position of the player taking into account collisions.

        """
        # How much overlap with a dimension of a surrounding block you need to
        # have to count as a collision. If 0, touching terrain at all counts as
        # a collision. If .49, you sink into the ground, as if walking through
        # tall grass. If >= .5, you'll fall through the ground.
        pad = 0.25
        p = list(position)
        np = normalize(position)
        for face in FACES:  # check all surrounding blocks
            for i in range(3):  # check each dimension independently
                if not face[i]:
                    continue
                # How much overlap you have with this dimension.
                d = (p[i] - np[i]) * face[i]
                if d < pad:
                    continue
                for dy in range(height):  # check each height
                    op = list(np)
                    op[1] -= dy
                    op[i] += face[i]
                    if tuple(op) not in self.parent.world.blocks:
                        continue
                    p[i] -= (d - pad) * face[i]
                    if face == (0, -1, 0) or face == (0, 1, 0):
                        # You are colliding with the ground or ceiling, so stop
                        # falling / rising.
                        self.dy = 0
                    break
        return tuple(p)

    def on_mouse_press(self, x, y, button, modifiers):
        """Called when a mouse button is pressed. See pyglet docs for button
        amd modifier mappings.

        Parameters
        ----------
        x, y : int
            The coordinates of the mouse click. Always center of the screen if
            the mouse is captured.
        button : int
            Number representing mouse button that was clicked. 1 = left button,
            4 = right button.
        modifiers : int
            Number representing any modifying keys that were pressed when the
            mouse button was clicked.

        """
        if self.exclusive:
            vector = self.get_sight_vector()
            block, previous = self.parent.world.hit_test(
                self.parent.avatar.position, vector
            )
            if button == mouse.LEFT:
                if previous:
                    self.parent.world.add_block(previous, self.block)
            elif button == pyglet.window.mouse.RIGHT and block:
                texture = self.parent.world.blocks[block]
                if texture in self.parent.world.inventory:
                    self.parent.world.remove_block(block)
        else:
            self.set_exclusive_mouse(True)

    def on_mouse_motion(self, x, y, dx, dy):
        """Called when the player moves the mouse.

        Parameters
        ----------
        x, y : int
            The coordinates of the mouse click. Always center of the screen if
            the mouse is captured.
        dx, dy : float
            The movement of the mouse.

        """
        if self.exclusive:
            m = 0.15
            x, y = self.rotation
            x, y = x + dx * m, y + dy * m
            y = max(-90, min(90, y))
            self.rotation = (x, y)

    def on_key_press(self, symbol, modifiers):
        if symbol == key.UP:
            self.strafe[0] -= 1
        elif symbol == key.DOWN:
            self.strafe[0] += 1
        elif symbol == key.LEFT:
            self.strafe[1] -= 1
        elif symbol == key.RIGHT:
            self.strafe[1] += 1
        elif symbol == key.SPACE:
            if self.dy == 0:
                self.dy = JUMP_SPEED
        elif symbol == key.J:
            self.jumping = not self.jumping
        elif symbol == key.ESCAPE:
            self.set_exclusive_mouse(False)
        elif symbol == key.TAB:
            self.flying = not self.flying
        elif symbol in self.num_keys:
            index = (symbol - self.num_keys[0]) % len(self.parent.world.inventory)
            self.block = self.parent.world.inventory[index]
        elif symbol == key.M:
            self.parent.voice.set_mic_toggle()

    def on_key_release(self, symbol, modifiers):
        if symbol == key.UP:
            self.strafe[0] += 1
        elif symbol == key.DOWN:
            self.strafe[0] -= 1
        elif symbol == key.LEFT:
            self.strafe[1] += 1
        elif symbol == key.RIGHT:
            self.strafe[1] -= 1

    def on_resize(self, width, height):
        self.info.y = height - 10
        # reticle
        if self.reticle:
            self.reticle.delete()
        x, y = self.width // 2, self.height // 2
        n = 10
        self.reticle = pyglet.graphics.vertex_list(
            4, ("v2i", (x - n, y, x + n, y, x, y - n, x, y + n))
        )

    def set_2d(self):
        width, height = self.get_size()
        gl.glDisable(gl.GL_DEPTH_TEST)
        viewport = self.get_viewport_size()
        gl.glViewport(0, 0, max(1, viewport[0]), max(1, viewport[1]))
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(0, max(1, width), 0, max(1, height), -1, 1)
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()

    def set_3d(self):
        width, height = self.get_size()
        gl.glEnable(gl.GL_DEPTH_TEST)
        viewport = self.get_viewport_size()
        gl.glViewport(0, 0, max(1, viewport[0]), max(1, viewport[1]))
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.gluPerspective(65.0, width / float(height), 0.1, 60.0)
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()
        x, y = self.rotation
        gl.glRotatef(x, 0, 1, 0)
        gl.glRotatef(-y, math.cos(math.radians(x)), 0, math.sin(math.radians(x)))
        x, y, z = self.parent.avatar.position
        gl.glTranslatef(-x, -y, -z)

    def on_draw(self):
        self.clear()

        self.set_3d()
        gl.glColor3d(1, 1, 1)
        self.parent.world.batch.draw()
        self.draw_focused_block()
        self.draw_avatars()

        self.set_2d()
        self.draw_info()
        self.draw_reticle()

    def draw_avatars(self):
        batch = pyglet.graphics.Batch()
        for username, data in dict(self.parent.others).items():
            if "position" in data:
                vertex_data = cube_vertices(*data["position"], 0.51 / 2)
                gl.glColor3d(1, 1, 1)
                gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)
                tex_coords_ = tex_coords(t=0, b=1, f=2, le=3, bk=4, ri=5, div=16)
                batch.add(
                    24,
                    gl.GL_QUADS,
                    self.parent.world.skin_tex_group["frog"],
                    ("v3f/static", vertex_data),
                    ("t2f/static", tex_coords_),
                )

        batch.draw()

    def draw_focused_block(self):
        """Draw black edges around the block that is currently under the
        crosshairs.

        """
        vector = self.get_sight_vector()
        block = self.parent.world.hit_test(self.parent.avatar.position, vector)[0]
        if block:
            x, y, z = block
            vertex_data = cube_vertices(x, y, z, 0.51)
            gl.glColor3d(0, 0, 0)
            gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)
            pyglet.graphics.draw(24, gl.GL_QUADS, ("v3f/static", vertex_data))
            gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)

    def draw_info(self):
        fps = float(pyglet.clock.get_fps())
        username = self.parent.username
        x, y, z = self.parent.avatar.position
        mic_status = "on" if self.parent.voice.mic_is_on else "off"
        block = self.block
        others = [x for x in self.parent.others.keys()]
        others = "[" + ", ".join(others) + "]"

        self.info.text = (
            f"Username: {username}\n"
            f"Position: {x:7.2f},{y:7.2f},{z:7.2f}\n"
            f"Frames/second: {fps:6.2f}\n"
            f"Mic: {mic_status}\n"
            f"Block: {block}\n"
            f"Others: {others}"
        )

        self.info.draw()

    def draw_reticle(self):
        """Draw the crosshairs in the center of the screen."""

        gl.glColor3d(0, 0, 0)
        self.reticle.draw(gl.GL_LINES)
