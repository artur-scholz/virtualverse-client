import time
import random
import pickle

from .config import SECTOR_SIZE


def get_timestamp():
    return time.time_ns()


# def create_world(floor_texture, wall_textures, n=80):
#     # n is 1/2 width and height of the world

#     blocks = {}

#     def add_block(position, texture):
#         blocks[position] = texture

#     s = 1  # step size
#     y = 0  # initial y height
#     for x in range(-n, n + 1, s):
#         for z in range(-n, n + 1, s):
#             # create boundary floor
#             add_block((x, y - 3, z), floor_texture)
#             # create another layer of (removable) blocks on top
#             add_block((x, y - 2, z), wall_textures[1])

#             if x in (-n, n) or z in (-n, n):
#                 # create outer walls.
#                 for dy in range(-2, 3):
#                     # walls are boundaries
#                     add_block((x, y + dy, z), floor_texture)

#     # generate the hills randomly
#     o = n - 10
#     for _ in range(120):
#         a = random.randint(-o, o)  # x position of the hill
#         b = random.randint(-o, o)  # z position of the hill
#         c = -1  # base of the hill
#         h = random.randint(1, 6)  # height of the hill
#         s = random.randint(4, 8)  # 2 * s is the side length of the hill
#         d = 1  # how quickly to taper off the hills
#         t = random.choice(wall_textures)
#         for y in range(c, c + h):
#             for x in range(a - s, a + s + 1):
#                 for z in range(b - s, b + s + 1):
#                     if (x - a) ** 2 + (z - b) ** 2 > (s + 1) ** 2:
#                         continue
#                     if (x - 0) ** 2 + (z - 0) ** 2 < 5**2:
#                         continue
#                     add_block((x, y, z), t)
#             s -= d  # decrement side lenth so hills taper off

#     return blocks


# def save_world(filename, blocks):
#     data = pickle.dumps(blocks)
#     with open(filename, "wb") as f:
#         f.write(data)


# def load_world(filename):
#     with open(filename, "rb") as f:
#         data = f.read()
#     return pickle.loads(data)


def cube_vertices(x, y, z, n):
    """Return the vertices of the cube at position x, y, z with size 2*n."""
    return [
        x - n,
        y + n,
        z - n,
        x - n,
        y + n,
        z + n,
        x + n,
        y + n,
        z + n,
        x + n,
        y + n,
        z - n,  # top
        x - n,
        y - n,
        z - n,
        x + n,
        y - n,
        z - n,
        x + n,
        y - n,
        z + n,
        x - n,
        y - n,
        z + n,  # bottom
        x - n,
        y - n,
        z - n,
        x - n,
        y - n,
        z + n,
        x - n,
        y + n,
        z + n,
        x - n,
        y + n,
        z - n,  # left
        x + n,
        y - n,
        z + n,
        x + n,
        y - n,
        z - n,
        x + n,
        y + n,
        z - n,
        x + n,
        y + n,
        z + n,  # right
        x - n,
        y - n,
        z + n,
        x + n,
        y - n,
        z + n,
        x + n,
        y + n,
        z + n,
        x - n,
        y + n,
        z + n,  # front
        x + n,
        y - n,
        z - n,
        x - n,
        y - n,
        z - n,
        x - n,
        y + n,
        z - n,
        x + n,
        y + n,
        z - n,  # back
    ]


def tex_coord(x, div=8):
    """Return the bounding vertices of the texture square."""
    mx = 1.0 / div
    dx = x * mx
    return dx, 0, dx + mx, 0, dx + mx, 1, dx, 1


def tex_coords(t=0, b=1, f=2, le=2, bk=2, ri=2, div=8):
    """Return a list of the texture squares for the top, bottom and side."""
    top = tex_coord(t, div)
    bottom = tex_coord(b, div)
    front = tex_coord(f, div)
    left = tex_coord(le, div)
    back = tex_coord(bk, div)
    right = tex_coord(ri, div)
    result = []
    result.extend(top)
    result.extend(bottom)
    result.extend(front)
    result.extend(left)
    result.extend(back)
    result.extend(right)
    return result


def normalize(position):
    """Accepts `position` of arbitrary precision and returns the block
    containing that position.

    Parameters
    ----------
    position : tuple of len 3

    Returns
    -------
    block_position : tuple of ints of len 3

    """
    x, y, z = position
    x, y, z = (int(round(x)), int(round(y)), int(round(z)))
    return (x, y, z)


def sectorize(position):
    """Returns a tuple representing the sector for the given `position`.

    Parameters
    ----------
    position : tuple of len 3

    Returns
    -------
    sector : tuple of len 3

    """
    x, y, z = normalize(position)
    x, y, z = x // SECTOR_SIZE, y // SECTOR_SIZE, z // SECTOR_SIZE
    return (x, 0, z)
