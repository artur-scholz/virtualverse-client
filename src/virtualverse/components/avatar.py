from virtualverse import AvatarPositionMessage, AvatarSkinMessage, get_timestamp
from .base import Component


class Avatar(Component):
    def __init__(self, parent):
        super().__init__(parent)

        self.position = None
        self.skin = None

    def process_msg(self, msg):
        if type(msg) == AvatarPositionMessage:
            if msg.username == self.parent.username:
                if self.position is None:
                    self.position = msg.position
            else:
                user = self.parent.others.get(msg.username)
                user["position"] = msg.position

        elif type(msg) == AvatarSkinMessage:
            if msg.username == self.parent.username:
                self.skin = msg.skin
            else:
                user = self.parent.others.get(msg.username)
                user["skin"] = msg.skin

    def send_position(self):
        msg = AvatarPositionMessage(
            get_timestamp(), self.parent.username, self.position
        )
        self.put_out_msg(msg)
