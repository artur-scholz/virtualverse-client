from virtualverse import ChatPrivateMessage, ChatBroadcastMessage, get_timestamp
from .base import Component


class Chat(Component):
    def process_msg(self, msg):
        if type(msg) == ChatPrivateMessage:
            if msg.username != self.parent.username:
                if msg.receiver == self.parent.username:
                    # TODO: show in window
                    print(f"{msg.username} says to you: {msg.text}")

        elif type(msg) == ChatBroadcastMessage:
            if msg.username != self.parent.username:
                print(f"{msg.username} says to all: {msg.text}")

    def send_private_text(self, receiver, text):
        msg = ChatPrivateMessage(get_timestamp(), self.parent.username, receiver, text)
        self.put_out_msg(msg)

    def send_broadcast_text(self, text):
        msg = ChatBroadcastMessage(get_timestamp(), self.parent.username, text)
        self.put_out_msg(msg)
