import queue
import threading


class Component:
    def __init__(self, parent):
        self.parent = parent

        self._msg_in = queue.Queue()
        self._msg_out = queue.Queue()

        self._thread = threading.Thread(target=self._loop, daemon=True)
        self._thread.start()

    def _loop(self):
        while True:
            msg = self._msg_in.get()
            self.process_msg(msg)

    def process_msg(self, msg):
        pass  # to be implemented by child class

    def put_out_msg(self, msg):
        self._msg_out.put(msg)

    def put_in_msg(self, msg):
        self._msg_in.put(msg)

    def get_in_msg(self):
        return self._msg_in.get()

    def get_out_msg(self):
        return self._msg_out.get()

    def has_out_msg(self):
        return not self._msg_out.empty()
