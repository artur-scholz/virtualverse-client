import os
import tempfile
import time
from collections import deque
from pathlib import Path

import pyglet
from pyglet import image
from pyglet import gl
from pyglet.graphics import TextureGroup


from virtualverse import WorldModifyMessage, get_timestamp
from virtualverse.config import FACES, TICKS_PER_SEC
from virtualverse.utils import cube_vertices, normalize, sectorize, tex_coords
from .base import Component


class World(Component):
    def __init__(self, parent):
        super().__init__(parent)

        self.blocks = {}
        self.shown = {}
        self._shown = {}
        self.sectors = {}
        self.block_tex_group = {}
        self.skin_tex_group = {}
        self.queue = deque()
        self.batch = pyglet.graphics.Batch()

        # save block textures in temp files, to be loaded by pyglet
        temp_dir = tempfile.TemporaryDirectory()
        temp_path = Path(temp_dir.name)
        texture_files = {}
        for name, data in parent.config["block-textures"].items():
            tex_filepath = os.path.join(temp_path, name + ".png")
            with open(tex_filepath, "wb") as f:
                f.write(data)
            texture_files[name] = tex_filepath
        self.inventory = list(texture_files.keys())[1:]
        for name, path in texture_files.items():
            self.block_tex_group[name] = TextureGroup(image.load(path).get_texture())
        try:
            temp_dir.cleanup()
        except PermissionError:
            pass

        # save skin textures in temp files, to be loaded by pyglet
        temp_dir = tempfile.TemporaryDirectory()
        temp_path = Path(temp_dir.name)
        texture_files = {}
        for name, data in parent.config["skin-textures"].items():
            tex_filepath = os.path.join(temp_path, name + ".png")
            with open(tex_filepath, "wb") as f:
                f.write(data)
            texture_files[name] = tex_filepath
        for name, path in texture_files.items():
            self.skin_tex_group[name] = TextureGroup(image.load(path).get_texture())
        try:
            temp_dir.cleanup()
        except PermissionError:
            pass

        for position, texture in parent.config["blocks"].items():
            self.add_block(position, texture, immediate=False, execute=True)

    def process_msg(self, msg):
        if type(msg) == WorldModifyMessage:
            funcname = msg.funcname
            func = getattr(self, funcname)
            args = msg.args
            kwargs = msg.kwargs
            func(*args, **kwargs, execute=True)

    def send_modify(self, funcname, *args, **kwargs):
        msg = WorldModifyMessage(
            get_timestamp(), self.parent.username, funcname, args, kwargs
        )
        self.put_out_msg(msg)

    def modification(func):
        def inner(*args, **kwargs):
            if "execute" in kwargs and kwargs["execute"] is True:
                del kwargs["execute"]
                func(*args, **kwargs)
            else:
                self = args[0]
                args = args[1:]
                funcname = func.__name__
                self.send_modify(funcname, *args, **kwargs)

        return inner

    @modification
    def add_block(self, position, texture, immediate=True):
        if position in self.blocks:
            self.remove_block(position, immediate)
        self.blocks[position] = texture
        self.sectors.setdefault(sectorize(position), []).append(position)
        if immediate:
            if self.exposed(position):
                self.show_block(position)
            self.check_neighbors(position)

    @modification
    def remove_block(self, position, immediate=True):
        try:
            del self.blocks[position]
        except KeyError:
            pass  # block is already deleted
        try:
            self.sectors[sectorize(position)].remove(position)
        except ValueError:
            pass  # block is already deleted
        if immediate:
            if position in self.shown:
                self.hide_block(position)
            self.check_neighbors(position)

    def hit_test(self, position, vector, max_distance=8):
        """Line of sight search from current position. If a block is
        intersected it is returned, along with the block previously in the line
        of sight. If no block is found, return None, None.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position to check visibility from.
        vector : tuple of len 3
            The line of sight vector.
        max_distance : int
            How many blocks away to search for a hit.

        """
        m = 8
        x, y, z = position
        dx, dy, dz = vector
        previous = None
        for _ in range(max_distance * m):
            key = normalize((x, y, z))
            if key != previous and key in self.blocks:
                return key, previous
            previous = key
            x, y, z = x + dx / m, y + dy / m, z + dz / m
        return None, None

    def exposed(self, position):
        """Returns False is given `position` is surrounded on all 6 sides by
        blocks, True otherwise.

        """
        x, y, z = position
        for dx, dy, dz in FACES:
            if (x + dx, y + dy, z + dz) not in self.blocks:
                return True
        return False

    def check_neighbors(self, position):
        """Check all blocks surrounding `position` and ensure their visual
        state is current. This means hiding blocks that are not exposed and
        ensuring that all exposed blocks are shown. Usually used after a block
        is added or removed.

        """
        x, y, z = position
        for dx, dy, dz in FACES:
            key = (x + dx, y + dy, z + dz)
            if key not in self.blocks:
                continue
            if self.exposed(key):
                if key not in self.shown:
                    self.show_block(key)
            else:
                if key in self.shown:
                    self.hide_block(key)

    def show_block(self, position, immediate=True):
        """Show the block at the given `position`. This method assumes the
        block has already been added with add_block()

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position of the block to show.
        immediate : bool
            Whether or not to show the block immediately.

        """
        texture = self.blocks.get(position)
        self.shown[position] = texture
        if immediate:
            self._show_block(position, texture)
        else:
            self._enqueue(self._show_block, position, texture)

    def _show_block(self, position, texture):
        """Private implementation of the `show_block()` method.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position of the block to show.
        texture : list of len 3
            The coordinates of the texture squares. Use `tex_coords()` to
            generate.

        """
        x, y, z = position
        vertex_data = cube_vertices(x, y, z, 0.5)
        # create vertex list
        # FIXME Maybe `add_indexed()` should be used instead
        self._shown[position] = self.batch.add(
            24,
            gl.GL_QUADS,
            self.block_tex_group[texture],
            ("v3f/static", vertex_data),
            ("t2f/static", tex_coords()),
        )

    def hide_block(self, position, immediate=True):
        """Hide the block at the given `position`. Hiding does not remove the
        block from the world.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position of the block to hide.
        immediate : bool
            Whether or not to immediately remove the block from the canvas.

        """
        self.shown.pop(position)
        if immediate:
            self._hide_block(position)
        else:
            self._enqueue(self._hide_block, position)

    def _hide_block(self, position):
        self._shown.pop(position).delete()

    def show_sector(self, sector):
        """Ensure all blocks in the given sector that should be shown are
        drawn to the canvas.

        """
        for position in self.sectors.get(sector, []):
            if position not in self.shown and self.exposed(position):
                self.show_block(position, True)

    def hide_sector(self, sector):
        """Ensure all blocks in the given sector that should be hidden are
        removed from the canvas.

        """
        for position in self.sectors.get(sector, []):
            if position in self.shown:
                self.hide_block(position, True)

    def change_sectors(self, before, after):
        """Move from sector `before` to sector `after`. A sector is a
        contiguous x, y sub-region of world. Sectors are used to speed up
        world rendering.

        """
        before_set = set()
        after_set = set()
        pad = 4
        for dx in range(-pad, pad + 1):
            for dy in [0]:  # range(-pad, pad + 1):
                for dz in range(-pad, pad + 1):
                    if dx**2 + dy**2 + dz**2 > (pad + 1) ** 2:
                        continue
                    if before:
                        x, y, z = before
                        before_set.add((x + dx, y + dy, z + dz))
                    if after:
                        x, y, z = after
                        after_set.add((x + dx, y + dy, z + dz))
        show = after_set - before_set
        hide = before_set - after_set
        for sector in show:
            self.show_sector(sector)
        for sector in hide:
            self.hide_sector(sector)

    def _enqueue(self, func, *args):
        self.queue.append((func, args))

    def _dequeue(self):
        func, args = self.queue.popleft()
        func(*args)

    def process_queue(self):
        """Process the entire queue while taking periodic breaks. This allows
        the game loop to run smoothly. The queue contains calls to
        _show_block() and _hide_block() so this method should be called if
        add_block() or remove_block() was called with immediate=False

        """
        start = time.time()
        while self.queue and time.time() - start < 1.0 / TICKS_PER_SEC:
            self._dequeue()

    def process_entire_queue(self):
        while self.queue:
            self._dequeue()
