from virtualverse import (
    get_timestamp,
    LoginResponseMessage,
    LogoutResponseMessage,
    LoginRequestMessage,
    LogoutRequestMessage,
    ConfigMessage,
)
from .base import Component


class Authenticate(Component):
    def __init__(self, parent):
        super().__init__(parent)

        self.logged_in = False

    def process_msg(self, msg):
        if type(msg) == LoginResponseMessage:
            if msg.username == self.parent.username:
                if not self.logged_in:
                    if msg.status is True:
                        self.logged_in = True
                        print("Logged in.")
                    else:
                        print("Failed to log in. Reason:", msg.text)
            else:
                if msg.status is True:
                    print(f"{msg.username} just logged in.")

        elif type(msg) == LogoutResponseMessage:
            if msg.username == self.parent.username:
                self.logged_in = False
            else:
                print(f"{msg.username} just logged out.")
                self.parent.others.pop(msg.username)

        elif type(msg) == ConfigMessage and self.logged_in:
            # check what data is received
            if msg.username == self.parent.username:
                if "blocks" in msg.config:
                    print("Receiving blocks...")
                if "block-textures" in msg.config:
                    print("Receiving block textures...")
                self.parent.config.update(msg.config)

                # check if all data received
                if (
                    "blocks" in self.parent.config
                    and "block-textures" in self.parent.config
                ):
                    self.parent.initialized = True

    def send_login(self):
        msg = LoginRequestMessage(
            get_timestamp(),
            self.parent.username,
            self.parent.password,
            self.parent.version,
        )
        self.put_out_msg(msg)

    def send_logout(self):
        msg = LogoutRequestMessage(get_timestamp(), self.parent.username)
        self.put_out_msg(msg)
