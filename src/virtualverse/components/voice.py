import time

from numpy.linalg import norm
import sounddevice as sd

from virtualverse import VoiceMessage, get_timestamp
from .base import Component


SAMPLE_RATE = 44100
BLOCKSIZE = 1024


class Voice(Component):
    def __init__(self, parent):
        super().__init__(parent)

        self.mic_is_on = False

        self.mic_threshold = self._determine_mic_threshold()
        if self.mic_threshold == 0:
            raise Exception()
        print(f"done [{self.mic_threshold}].")

        self._mic_stream = sd.InputStream(
            channels=1,
            samplerate=SAMPLE_RATE,
            dtype="int16",
            blocksize=BLOCKSIZE,
            callback=self._mic_callback,
        )

        self._speaker_stream = sd.OutputStream(
            channels=1, samplerate=SAMPLE_RATE, dtype="int16", blocksize=BLOCKSIZE
        )
        self._speaker_stream.start()

    def process_msg(self, msg):
        if type(msg) == VoiceMessage:
            if msg.username == self.parent.username:
                pass  # suppress echo
            else:
                self._speaker_stream.write(msg.data)

    def send_voice(self, data):
        msg = VoiceMessage(get_timestamp(), self.parent.username, data)
        self.put_out_msg(msg)

    def set_mic_on(self):
        self._mic_stream.start()
        self.mic_is_on = True
        print("Mic is on.")

    def set_mic_off(self):
        self._mic_stream.stop()
        self.mic_is_on = False
        print("Mic is off.")

    def set_mic_toggle(self):
        if self.mic_is_on:
            self.set_mic_off()
        else:
            self.set_mic_on()

    def _determine_mic_threshold(self):
        """Determine the microphone threshold by recording initial silence."""
        buffer = []

        def mic_threshold_callback(in_data, frame_count, time_info, status):
            from numpy.linalg import norm

            buffer.append(norm(in_data))

        mic_stream = sd.InputStream(
            channels=1,
            samplerate=SAMPLE_RATE,
            dtype="int16",
            blocksize=BLOCKSIZE,
            callback=mic_threshold_callback,
        )

        print("Determining mic threshold...", end="")
        mic_stream.start()
        time.sleep(1)
        mic_stream.stop()
        return 2.0 * (sum(buffer) / len(buffer))

    def _mic_callback(self, in_data, frame_count, time_info, status):
        if norm(in_data) > self.mic_threshold:
            self.send_voice(in_data)
