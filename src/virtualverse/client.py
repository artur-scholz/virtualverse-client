import threading
import time

from .messages import (
    LoginResponseMessage,
    WorldModifyMessage,
    ChatPrivateMessage,
    VoiceMessage,
    AvatarPositionMessage,
    ChatBroadcastMessage,
    LogoutResponseMessage,
    ConfigMessage,
)
from .components import Authenticate, Avatar, Chat, Voice, World
from .transport.ws import WsTransport as Transport


class Client:
    version = "0.0.1"

    def __init__(self, username, password, host, port):
        self.username = username
        self.password = password

        self.config = {
            "blocks": {},
            "block-textures": {},
            "skin-textures": {},
        }
        self.initialized = False
        self.others = {
            # username: {
            #     'position':
            #     'skin':
            # }
        }

        self.authenticate = Authenticate(self)
        self.avatar = Avatar(self)
        self.chat = Chat(self)
        self.voice = Voice(self)
        self.world = None

        self.transport = Transport(host, port)

        _thread = threading.Thread(target=self._msg_in, daemon=True)
        _thread.start()
        _thread = threading.Thread(target=self._msg_out, daemon=True)
        _thread.start()

    def shutdown(self):
        self.transport.shutdown()

    def login(self):
        self.authenticate.send_login()

    def logout(self):
        self.authenticate.send_logout()

    def is_logged_in(self):
        return self.authenticate.logged_in

    def configure(self):
        self.world = World(self)

    def _msg_in(self):
        while True:
            msg = self.transport.get_in_msg()

            # user not yet known, add to self.others
            if msg.username != self.username and msg.username not in self.others:
                self.others[msg.username] = {}

            if type(msg) in [
                LoginResponseMessage,
                LogoutResponseMessage,
                ConfigMessage,
            ]:
                self.authenticate.put_in_msg(msg)

            if self.is_logged_in():
                if self.world and type(msg) == WorldModifyMessage:
                    self.world.put_in_msg(msg)
                elif type(msg) == AvatarPositionMessage:
                    self.avatar.put_in_msg(msg)
                elif type(msg) in [ChatPrivateMessage, ChatBroadcastMessage]:
                    self.chat.put_in_msg(msg)
                elif type(msg) == VoiceMessage:
                    self.voice.put_in_msg(msg)

    def _msg_out(self):
        while True:
            if self.authenticate.has_out_msg():
                msg = self.authenticate.get_out_msg()
                self.transport.put_out_msg(msg)
            elif self.avatar.has_out_msg():
                msg = self.avatar.get_out_msg()
                self.transport.put_out_msg(msg)
            elif self.chat.has_out_msg():
                msg = self.chat.get_out_msg()
                self.transport.put_out_msg(msg)
            elif self.voice.has_out_msg():
                msg = self.voice.get_out_msg()
                self.transport.put_out_msg(msg)
            elif self.world and self.world.has_out_msg():
                msg = self.world.get_out_msg()
                self.transport.put_out_msg(msg)
            else:
                time.sleep(0.01)  # no messages in queue, add a delay
