import queue


class Transport:
    def __init__(self, host, port):
        self.host = host
        self.port = port

        self._msg_in = queue.Queue()
        self._msg_out = queue.Queue()

    def shutdown(self):
        pass

    def put_out_msg(self, msg):
        self._msg_out.put(msg)

    def put_in_msg(self, msg):
        self._msg_in.put(msg)

    def get_in_msg(self):
        return self._msg_in.get()

    def get_out_msg(self):
        return self._msg_out.get()

    def has_out_msg(self):
        return not self._msg_out.empty()
