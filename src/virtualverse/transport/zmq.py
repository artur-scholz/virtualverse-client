import threading

import zmq

from .base import Transport


class ZmqTransport(Transport):
    def __init__(self, host, port):
        super().__init__(host, port)

        self._context = zmq.Context()

        self._socket_out = self._context.socket(zmq.PUSH)
        self._socket_out.connect("tcp://{}:{}".format(host, port))

        self._socket_in = self._context.socket(zmq.SUB)
        self._socket_in.connect("tcp://{}:{}".format(host, port + 1))
        self._socket_in.setsockopt(zmq.SUBSCRIBE, b"")
        self._poller = zmq.Poller()
        self._poller.register(self._socket_in, zmq.POLLIN)

        _thread = threading.Thread(target=self._send, daemon=True)
        _thread.start()
        _thread = threading.Thread(target=self._receive, daemon=True)
        _thread.start()

    def _send(self):
        while True:
            msg = self.get_out_msg()
            print("msg out:", type(msg))
            try:
                self._socket_out.send_pyobj(msg, copy=True)
            except zmq.error.ZMQError:
                pass

    def _receive(self):
        while True:
            try:
                sockets = dict(self._poller.poll(1000))
                for socket, event in sockets.items():
                    msg = self._socket_in.recv_pyobj()
                    print("msg in:", type(msg))
                    self.put_in_msg(msg)
            except Exception:
                pass
