import threading
import socket
import pickle

from .base import Transport


MAX_FRAME_SIZE = 64 * 1024


class UdpTransport(Transport):
    def __init__(self, host, port):
        super().__init__(host, port)

        self._socket = socket.socket(
            socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP
        )
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self._socket.bind(("", 0))

        _thread = threading.Thread(target=self._send, daemon=True)
        _thread.start()
        _thread = threading.Thread(target=self._receive, daemon=True)
        _thread.start()

    def _send(self):
        while True:
            msg = self.get_out_msg()
            print("msg out:", type(msg))
            data = pickle.dumps(msg)
            self._socket.sendto(data, (self.host, self.port))

    def _receive(self):
        while True:
            data, addr = self._socket.recvfrom(MAX_FRAME_SIZE)
            msg = pickle.loads(data)
            print("msg in:", type(msg))
            self.put_in_msg(msg)
