import pickle
import threading
import asyncio

import websockets

from .base import Transport


class WsTransport(Transport):
    def __init__(self, host, port):
        super().__init__(host, port)

        async def main(self):
            websocket = await websockets.connect(f"ws://{self.host}:{self.port}")
            task1 = asyncio.create_task(self._send(websocket))
            task2 = asyncio.create_task(self._receive(websocket))
            await task1
            await task2

        def _loop(loop):
            asyncio.set_event_loop(loop)
            loop.run_forever()

        loop = asyncio.new_event_loop()
        t = threading.Thread(target=_loop, args=(loop,), daemon=True)
        t.start()
        asyncio.run_coroutine_threadsafe(main(self), loop)

    async def _send(self, websocket):
        while True:
            while self.has_out_msg():
                msg = self.get_out_msg()
                print(f"← ({msg.timestamp}, {msg.username}, {type(msg)})")
                data = pickle.dumps(msg)
                await websocket.send(data)
            else:
                await asyncio.sleep(0.01)

    async def _receive(self, websocket):
        async for data in websocket:
            msg = pickle.loads(data)
            print(f"→ ({msg.timestamp}, {msg.username}, {type(msg)})")
            self.put_in_msg(msg)
