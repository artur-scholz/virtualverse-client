# VirtualVerse

Meet with your friends, family, colleagues in this virtual world where you can
create, discuss, chat, collaborate, play, and socialize.

![screenshot](docs/image2.png)

VirtualVerse is inspired my voxel games like [Minetest](https://www.minetest.net/) and brings the following unique features:
- walk freely in a 3D voxel world and build/destroy blocks
- voice chat with other logged in users
- voice volume is adjusted based on distances between users
- fly modus for free navigation (default is gravity on)

To be added:
- upload and display of slides ("presentation feature")
- draw and write on boards
- streaming of videos ("cinema feature")

## Getting Started

### On Windows

Download and run the executable [vv-client.exe](https://gitlab.com/virtualverse/virtualverse-client/-/blob/main/vv-client.exe).

### Using Python

Install via Pip:

```
mkdir virtualverse
cd virtualverse
python -m venv venv
source venv
pip install git+https://gitlab.com/virtualverse/virtualverse-client.git
```

Start client to connect to demo server:
```
vv-client
```

## Game Controls

Use the mouse to look around and build/destroy blocks (left/right click).

The keyboard controls are:
- UP        move forward
- DOWN      move backward
- LEFT      move left
- RIGHT     move right
- M         toggle microphone on/off
- 1-4       select block material
- TAB       toggle flying mode
- SPACE     jump
- ESC       exit window mode

## Development Setup

```
git clone https://gitlab.com/virtualverse/virtualverse-client.git
cd virtualverse-client
python3 -m venv venv
source venv/bin/activate
pip install -e .
```

The system follows a client-server architecture. Clients send messages to the server and receive broadcast messages from the server. Thus, clients never communicate with each other directly, but all communication goes over the server. Websockets are used as messaging technology.

Join the chat: https://app.element.io/#/room/#virtualverse:matrix.org
